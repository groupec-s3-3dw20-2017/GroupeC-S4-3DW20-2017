<?php
	// On inclut le chemin vers le fichier Autoloader.php et on indique le fichier où se trouvent les templates
	include_once('lib/Twig/Autoloader.php');

	Twig_Autoloader::register(); // Permet le chargement automatique de la classe Twig_Autoloader

	$templates = new Twig_Loader_Filesystem('templates'); // Dossier contenant les templates
	$twig      = new Twig_Environment($templates); // Initialisation de l'environnement Twig
?>