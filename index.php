<?php
	// On inclut le fichier inc.twig.php
	include 'inc.twig.php'; 
	// Déclaration des variables $template_index (qui permet de charger le template index.tpl), $n_jours_previsions et $ville
	$template_index = $twig->loadTemplate('index.tpl'); 
	$n_jours_previsions = 3;
	$ville = "Limoges";

	//~ Clé API
	//~ Si besoin, vous pouvez générer votre propre clé d'API gratuitement, en créant 
	//~ votre propre compte ici : https://home.openweathermap.org/users/sign_up
	$apikey = "10eb2d60d4f267c79acb4814e95bc7dc";
	// Permet d'accéder au site api.openweathermap qui contient les données météorologiques
	$data_url = 'http://api.openweathermap.org/data/2.5/forecast/daily?APPID='.$apikey.'&q='.$ville.',fr&lang=fr&units=metric&cnt='.$n_jours_previsions;

	//Déclaration des variables permettant des récupérer le contenu du site spécifié ci-dessus 
	$data_contenu = file_get_contents($data_url);
	$_data_array = json_decode($data_contenu, true); // Permet de convertir une chaîne encodée JSON en une variable PHP
	$_ville = $_data_array['city'];
	$_journees_meteo = $_data_array['list']; 

	// Affichage des données météorologiques tant que i est inférieur à la variable $_journees_meteo donc à 3
	for ($i = 0; $i < count($_journees_meteo); $i++) 
	{
		$_meteo = getMeteoImage($_journees_meteo[$i]['weather'][0]['icon']);	
		$_journees_meteo[$i]['meteo'] = $_meteo;
	}

	// Affichage des données contenues dans les variables via le fichier index.tpl
	echo $template_index->render(array(
		'_journees_meteo'	=> $_journees_meteo,
		'_ville'			=> $_ville, 
		'n_jours_previsions'=> $n_jours_previsions
	));

	// Affichage des pictogrammes relatifs au temps (soleil, lune, pluie...)
	function getMeteoImage($code){
		if(strpos($code, 'n')) 
		{
			return 'entypo-moon'; // Permet d'afficher le pictogramme "lune" la nuit
		}
		
		//~ Définition de la variable $_icones_meteo contenant, sous forme d'un tableau, les correspondances entre les pictogrammes 
		//~ et les "icon" (codes de 3 caractères définissant une condition météorologique)
		$_icones_meteo = array(
			'01d' => 'entypo-light-up',
			'02d' => 'entypo-light-up',
			'03d' => 'entypo-cloud', 
			'04d' => 'entypo-cloud',
			'09d' => 'entypo-water', 
			'10d' => 'entypo-water',
			'11d' => 'entypo-flash',
			'13d' => 'entypo-star', 
			'50d' => 'entypo-air'
		);

		// Affichage, uniquement si elles existent, des données contenues dans les variables $code et $_icones_meteo 
		if(array_key_exists($code, $_icones_meteo)) 
		{
			return $_icones_meteo[$code];
		}
		else
		{
			return 'entypo-help';
		}
	}
?>
