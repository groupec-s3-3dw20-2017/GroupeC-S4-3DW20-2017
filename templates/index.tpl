{{ include('top.tpl') }} {# On inclut le fichier top.tpl #}

<div class="preloader">
	<div class="preloader-top">
		<div class="preloader-top-sun">
			<div class="preloader-top-sun-bg"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-0"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-45"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-90"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-135"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-180"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-225"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-270"></div>
			<div class="preloader-top-sun-line preloader-top-sun-line-315"></div>
		</div>
	</div>
	<div class="preloader-bottom">
		<div class="preloader-bottom-line preloader-bottom-line-lg"></div>
		<div class="preloader-bottom-line preloader-bottom-line-md"></div>
		<div class="preloader-bottom-line preloader-bottom-line-sm"></div>
		<div class="preloader-bottom-line preloader-bottom-line-xs"></div>
	</div>
</div>
<div class="wrapper">
	<section class="bordure">
	    <p>Météo</p>
	</section>

	<section class="contenu">
	    <h1>
	    	{# Affichage nom de la ville avec première lettre en majuscule et affichage du pays en majuscules #}
	    	{{_ville.name|capitalize}}, {{_ville.country|upper}} 
	    	<a href="http://maps.google.com/maps?q={{_ville.coord.lat}},{{_ville.coord.lon}}" class="lk" target="_blank" title="Voir sur une carte">
	    		Voir sur une carte
	    	</a>
	    </h1>
	    {# Affichage des données contenues dans la variable _journees_meteo dans le fichier index.php #}
	    {% for journee in _journees_meteo %} 
	    	<div class="jour">
	    		<div class="numero_jour">
	    			<h2>Météo du {{journee.dt|date('d/m/Y')}}</h2> {# Affichage de la date avec le format jour/mois/année #}
	    		</div>

			    <div class="temperature {{journee.meteo}}">
			      {# Affichage de la température qu'il fera le jour indiqué #}
			      <h2>{{journee.temp.day}}<span class="degree-symbol">°</span>C</h2>
			    </div>

			    <ul>
			      <li class="fontawesome-leaf left">
			      	{# Affichage de la vitesse du vent qu'il y aura le jour indiqué #}
			        <span>{{journee.speed}} km/h</span>
			      </li>
			      <li class="fontawesome-tint center">
			      	{# Affichage du taux d'humidité qu'il y aura le jour indiqué #}
			        <span>{{journee.humidity}}%</span>
			      </li>
			      <li class="fontawesome-dashboard right">
			      	{# Affichage de la pression de l'air qu'il y aura le jour indiqué #}
			        <span>{{journee.pressure}}</span>
			      </li>
			    </ul> 
			    <div class="description">
			    	Description : {{journee.weather|first.description|capitalize}} {# Affichage de "Description :" et du temps correspondant avec la première lettre en majuscule #}
			    </div>
			</div>
	    {% endfor %} {# Fin de la boucle for #}

	    <div class="bullets">
	    	{% for i in 1..n_jours_previsions %} {# Début de la boucle for permettant d'afficher les puces rondes cliquables #}
	    		<span class="entypo-record" data-cible="{{i-1}}"></span>
	    	{% endfor %} {# Fin de la boucle for #}
	    </div>

	</section>
</div>
{{ include('bottom.tpl') }} {# On inclut le fichier bottom.tpl #}